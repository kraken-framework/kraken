﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.runtime
{
    public interface IPlugin
    {
        Task LoadAsync(IApplicationContext context, CancellationToken ct);
    }

    public interface IPluginStart
    {
        // runs after all plugins have been loaded but before app starts
        Task BeforeStartAsync(IApplicationContext context, CancellationToken ct);
        // runs after app has been started
        Task AfterStartAsync(IApplicationContext context, CancellationToken ct);
    }

    public interface IPluginStop
    {
        // runs before app will be stopped
        Task BeforeStopAsync(IApplicationContext context, CancellationToken ct);
        // runs after app has been stopped
        Task AfterStopAsync(IApplicationContext context, CancellationToken ct);
    }
}
