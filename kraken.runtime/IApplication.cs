﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.runtime
{
    public interface IApplication
    {
        Task<IApplicationContext> InitAsync(CancellationToken ct);
        Task StartAsync(IApplicationContext context, CancellationToken ct);
        Task StopAsync(IApplicationContext context, CancellationToken ct);

    }
}
