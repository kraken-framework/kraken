﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.runtime
{
    public enum Scope
    {
        Singleton,
        Transient,
    }
    public interface IApplicationContext
    {
        void Use(Type serviceType, Type implementationType, Scope scope = Scope.Transient);
        void Use<TService, TImplementation>(Scope scope = Scope.Transient)
            where TService : class
            where TImplementation : class, TService;
        void Register(Type serviceType, Type implementationType, Scope scope = Scope.Transient);
        void Register<TService, TImplementation>(Scope scope = Scope.Transient)
            where TService : class
            where TImplementation : class, TService;
        TService? GetService<TService>();
        object? GetService(Type type);
        IEnumerable<TService> GetServices<TService>();
        IEnumerable<object?> GetServices(Type type);
    }
}
