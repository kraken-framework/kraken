﻿
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Loader;

namespace kraken
{
    public class BootStrapper
    {
        private string baseDirectory;
        private string id;

        public BootStrapper(string baseDirectory, string id)
        {
            this.baseDirectory = baseDirectory;
            this.id = id;
        }

        public async Task RunAsync(string[] args, CancellationToken ct)
        {
            Directory.SetCurrentDirectory(baseDirectory);

            // Create a new AssemblyLoadContext
            var binDirectory = Path.Combine(baseDirectory, "bin");
            var loadContext = new CustomLoadContext(binDirectory);

            // Load the default assembly into the new context
            var entryAssemblyFile = Path.Combine(binDirectory, id + ".dll");
            Console.WriteLine("Loading {0}", entryAssemblyFile);
            var entryAssembly = loadContext.LoadFromAssemblyPath(Path.Combine(binDirectory, id + ".dll"));
            
            // load all other assemblies from the bin directory
            foreach (var assemblyFile in Directory.GetFiles(binDirectory, "*.dll"))
            {
                Console.WriteLine("Loading {0}", Path.GetFileName(assemblyFile));
                loadContext.LoadFromAssemblyPath(assemblyFile);
            }

            // detect application
            var applicationType = entryAssembly.GetTypes().SingleOrDefault(x => x.GetInterface("IApplication") != null) ?? throw new ArgumentException("Unable to determine application in entry assembly");
            dynamic application = Activator.CreateInstance(applicationType, new object[] { args }) ?? throw new ArgumentException("Unable to create instance");

            var applicationContext = await application.InitAsync(ct);

            var plugins = loadContext
                .Assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => x.GetInterface("IPlugin") != null)
                .Select(x => Activator.CreateInstance(x) ?? throw new ArgumentException($"Unable to create plugin instance for {x.FullName}"))
                .ToArray();

            foreach (dynamic plugin in plugins)
            {
                await plugin.LoadAsync(applicationContext, ct);
            }

            foreach (dynamic plugin in plugins.Where(x => x.GetType().GetMethod("BeforeStartAsync") != null))
            {
                await plugin.BeforeStartAsync(applicationContext, ct);
            }

            // Invoke a method on the instance
            await application.StartAsync(applicationContext, ct);

            foreach (dynamic plugin in plugins.Where(x => x.GetType().GetMethod("AfterStartAsync") != null))
            {

                await plugin.AfterStartAsync(applicationContext, ct);
            }

            ct.WaitHandle.WaitOne();

            foreach (dynamic plugin in plugins.Where(x => x.GetType().GetMethod("BeforeStopAsync") != null))
            {
                await plugin.BeforeStopAsync(applicationContext, ct);
            }

            await application.StopAsync(applicationContext, ct);

            foreach (dynamic plugin in plugins.Where(x => x.GetType().GetMethod("AfterStopAsync") != null)) 
            {
                await plugin.AfterStopAsync(applicationContext, ct);
            }
        }


        class CustomLoadContext : AssemblyLoadContext
        {
            private readonly string baseDirectory;

            // Custom load context that isolates the loading of assemblies
            public CustomLoadContext(string baseDirectory) : base(isCollectible: true)
            {
                this.baseDirectory = baseDirectory;
            }

            // Override the Load method if you need to customize the loading process
            protected override Assembly? Load(AssemblyName assemblyName)
            {
                var assemblyPath = Path.Combine(baseDirectory, assemblyName.Name + ".dll");
                if (File.Exists(assemblyPath))
                {
                    Console.WriteLine("-->Assembly found at {0}", assemblyPath);
                    return Assembly.LoadFrom(assemblyPath);
                }
                //return Assembly.Load(assemblyName);
                return null; // Default behavior, can customize as needed
            }
        }

    }
}