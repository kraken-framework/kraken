﻿using System.Xml.Serialization;

namespace kraken.Models
{
    [XmlRoot(ElementName = "App")]
    public class App
    {
        public string Id { get; set; } = null!;
        public string Version { get; set; } = null!;
    }
}