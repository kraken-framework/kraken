﻿using System.Collections;
using System.Xml.Serialization;

namespace kraken.Models
{
    [XmlRoot(ElementName = "packages")]
    public class PackageList
    {
        [XmlElement(ElementName = "package")]
        public List<Package> Packages { get; set; } = new List<Package>();
    }

    [XmlRoot(ElementName = "package")]
    public class Package
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; } = null!;
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; } = null!;
        [XmlAttribute(AttributeName = "targetFramework")]
        public string TargetFramework { get; set; } = null!;
        public override string ToString()
        {
            return $"{Id} {Version}";
        }
    }
}