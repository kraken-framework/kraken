﻿using NuGet.Configuration;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace kraken.Models
{

    // dotnet nuget list source --configfile=NuGet.Config
    // dotnet nuget enable source kraken --configfile=NuGet.Config
    // dotnet nuget add source https://gitlab.com/api/v4/projects/.../packages/nuget/index.json --name=test --username=user --password=pass --store-password-in-clear-text --configfile=NuGet.Config
    // dotnet nuget remove source kraken --configfile=NuGet.Config
    public class NuGetConfig
    {
        public PackageSources PackageSources { get; set; } = null!;
        public DisabledPackageSources DisabledPackageSources { get; set; } = null!;
        public Dictionary<string, Credentials> PackageSourceCredentials { get; set; } = null!;

        public static readonly NuGetConfig Default = new NuGetConfig
        {
            PackageSources = new PackageSources
            {
                Sources = new List<Entry>
                {
                    new Entry
                    {
                        Key = "nuget.org",
                        Value = "https://api.nuget.org/v3/index.json",
                    }
                }
            },
            DisabledPackageSources = new DisabledPackageSources
            {
                Sources = new List<Entry>(),
            },
            PackageSourceCredentials = new Dictionary<string, Credentials>()
            {
               
            }
        };
            

        public static NuGetConfig Deserialize(string path)
        {
            if (!File.Exists(path))
            {
                return NuGetConfig.Default;
            }
            var doc = XDocument.Load(path);

            var config = new NuGetConfig
            {
                PackageSources = new PackageSources
                {
                    Sources = doc.Descendants("packageSources").Elements("add").Select(x => new Entry
                    {
                        Key = x?.Attribute("key")?.Value ?? "",
                        Value = x?.Attribute("value")?.Value ?? ""
                    }).ToList()
                },
                DisabledPackageSources = new DisabledPackageSources
                {
                    Sources = doc.Descendants("disabledPackageSources").Elements("add").Select(x => new Entry
                    {
                        Key = x?.Attribute("key")?.Value ?? "",
                        Value = x?.Attribute("value")?.Value ?? ""
                    }).ToList()
                },
                PackageSourceCredentials = doc.Descendants("packageSourceCredentials").Elements().ToDictionary(
                    d => d.Name.LocalName,
                    d => new Credentials
                    {
                        CredentialList = d.Elements("add").Select(x => new Entry
                        {
                            Key = x?.Attribute("key")?.Value ?? "",
                            Value = x?.Attribute("value")?.Value ?? ""
                        }).ToList()
                    }, StringComparer.OrdinalIgnoreCase
                )
            };

            return config;
        }

        public static IEnumerable<SourceRepository> GetSourceRepositories(NuGetConfig nugetConfig)
        {
            var repos = new List<SourceRepository>();
            foreach (var source in nugetConfig.PackageSources.Sources)
            {
                var isDisabled = bool.Parse(nugetConfig.DisabledPackageSources.Sources.SingleOrDefault(x => x.Key.Equals(source.Key, StringComparison.OrdinalIgnoreCase))?.Value ?? "False");
                if (!isDisabled)
                {
                    var credential = GetCredential(nugetConfig, source);
                    var repo = GetSourceRepository(source.Value, source.Key, credential);
                    repos.Add(repo);
                }

            }
            return repos;
        }

        private static PackageSourceCredential? GetCredential(NuGetConfig nugetConfig, Entry source)
        {
            if (nugetConfig.PackageSourceCredentials.TryGetValue(source.Key, out var credentials))
            {
                var userName = credentials.CredentialList.Single(x => x.Key == "Username")?.Value;
                var password = credentials.CredentialList.SingleOrDefault(x => x.Key == "Password")?.Value;
                var isPasswordClearText = false;
                if (password == null)
                {
                    password = credentials.CredentialList.SingleOrDefault(x => x.Key == "ClearTextPassword")?.Value;
                    isPasswordClearText = true;
                }
                return new PackageSourceCredential(source.Value, userName, password, isPasswordClearText, null);
            }
            return null;
        }

        public static SourceRepository GetSourceRepository(string path)
        {
            return GetSourceRepository(path, "Local");
        }

        public static SourceRepository GetSourceRepository(string source, string name, PackageSourceCredential? credential = null)
        {
            var packageSource = new PackageSource(source, name)
            {
                Credentials = credential
            };
            var repo = Repository.Factory.GetCoreV3(packageSource);
            return repo;
        }
    }

    public class PackageSources
    {
        public List<Entry> Sources { get; set; } = new List<Entry>();
    }

    public class DisabledPackageSources
    {
        public List<Entry> Sources { get; set; } = new List<Entry>();
    }

    public class Entry
    {
        public string Key { get; set; } = null!;
        public string Value { get; set; } = null!;
        public override string ToString()
        {
            return new { Key, Value }.ToString()!;
        }
    }

    public class Credentials
    {
        public List<Entry> CredentialList { get; set; } = new List<Entry>();
    }

    
    public class XmlDeserializer
    {
        public static NuGetConfig Deserialize(string fileName)
        {
            var doc = XDocument.Load(fileName);

            var config = new NuGetConfig
            {
                PackageSources = new PackageSources
                {
                    Sources = doc.Descendants("packageSources").Elements("add").Select(x => new Entry
                    {
                        Key = x?.Attribute("key")?.Value ?? "",
                        Value = x?.Attribute("value")?.Value ?? ""
                    }).ToList()
                },
                DisabledPackageSources = new DisabledPackageSources
                {
                    Sources = doc.Descendants("disabledPackageSources").Elements("add").Select(x => new Entry
                    {
                        Key = x?.Attribute("key")?.Value ?? "",
                        Value = x?.Attribute("value")?.Value ?? ""
                    }).ToList()
                },
                PackageSourceCredentials = doc.Descendants("packageSourceCredentials").Elements().ToDictionary(
                    d => d.Name.LocalName,
                    d => new Credentials
                    {
                        CredentialList = d.Elements("add").Select(x => new Entry
                        {
                            Key = x?.Attribute("key")?.Value ?? "",
                            Value = x?.Attribute("value")?.Value ?? ""
                        }).ToList()
                    }, StringComparer.OrdinalIgnoreCase
                )
            };

            return config;
        }
    }
}
