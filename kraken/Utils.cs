﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using NuGet.Frameworks;
using System.Reflection;
using System.Runtime.Versioning;

namespace kraken
{
    internal class Utils
    {
        internal static T DeserializeXml<T>(string path)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = File.OpenText(path))
            {
                return (T)(serializer.Deserialize(reader) ?? throw new ArgumentNullException());
            }
        }

        internal static void SerializeXml<T>(T model, string path)
        {
            var serializer = new XmlSerializer(typeof(T));
            var namespaces = new XmlSerializerNamespaces([XmlQualifiedName.Empty]);

            using (var writer = File.CreateText(path))
            {
                serializer.Serialize(writer, model, namespaces);
            }
        }

        internal static NuGetFramework GetTargetFramework()
        {
            // Target framework for your application/library
            var assembly = Assembly.GetExecutingAssembly();
            var targetFrameworkAttribute = assembly.GetCustomAttribute<TargetFrameworkAttribute>() ?? throw new NotSupportedException();
            var targetFrameworkName = targetFrameworkAttribute.FrameworkName;
            var targetFramework = NuGetFramework.Parse(targetFrameworkName); // Adjust as per your target framework
            return targetFramework;
        }
    }
}
