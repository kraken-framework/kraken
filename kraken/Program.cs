using kraken.CommandHandlers;
using kraken.Models;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Parsing;
using System.Text;
using System.Threading;

namespace kraken
{
    public class Program
    {
        public static async Task<int> Main(string[] args)
        {
            // [ ] kraken init      -> neues kraken-root erstellen
            // [x] kraken list      -> alle apps auflisten
            // [x] kraken add       -> neue app hinzuf�gen
            // [x] kraken run       -> app ausf�hren
            // [x] kraken update    -> app packages aktualisieren
            // [x] kraken upgrade   -> app aktualisieren
            // [ ] kraken install   -> app als Dienst installieren
            // [ ] kraken start     -> dienst starten
            // [ ] kraken stop      -> dienst stoppen
            // [ ] kraken restart   -> dienst neu starten
            // [ ] kraken uninstall -> dienst deinstallieren
            // [ ] kraken remove    -> app entfernen

            // TODO: Service install / uninstall
            // TODO: Pack as tool https://learn.microsoft.com/de-de/dotnet/core/tools/global-tools https://dev.to/karenpayneoregon/c-net-tools-withsystemcommandline-2nc2
            // TODO: Bug: update/upgrade ignores new dependencies
            // TODO: Source Mapping support https://learn.microsoft.com/en-us/nuget/release-notes/nuget-6.0#source-mapping

            var cts = new CancellationTokenSource();
            var ct = cts.Token;
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                // Prevent the application from terminating immediately
                eventArgs.Cancel = true;
                cts.Cancel();
                Console.WriteLine("Cancellation requested.");
            };

            // TODO: M�glichkeit schaffen --root als Argument zu �bergeben,
            //       Verzeichnisse / PackageManager erst nach der Auswertung von --root erstellen
            // Alternativ --root:... hier aus args auswerten und entfernen
            // Wird ben�tigt f�r service, damit man mehrere roots haben kann
            var rootDirectory = Environment.GetEnvironmentVariable("KRAKEN_ROOT") ?? Path.GetFullPath(".");
            Directory.CreateDirectory(rootDirectory);

            var appsDirectory = Path.Combine(rootDirectory, "apps");
            Directory.CreateDirectory(appsDirectory);

            var packagesDirectory = Path.Combine(rootDirectory, "packages");
            Directory.CreateDirectory(packagesDirectory);

            var targetFramework = Utils.GetTargetFramework();
            var nugetConfigFile = Path.Combine(rootDirectory, "NuGet.Config");
            var nugetConfig = NuGetConfig.Deserialize(nugetConfigFile);
            var sourceRepositories = NuGetConfig.GetSourceRepositories(nugetConfig);
            var packagesRepository = NuGetConfig.GetSourceRepository(packagesDirectory);
            var packageManager = new PackageManager(
                targetFramework,
                appsDirectory,
                packagesRepository,
                sourceRepositories.ToArray());

            var nameArgument = new Argument<string>(
                name: "name",
                description: "set name");
            //getDefaultValue: () => "default");

            //var idOption = new Option<string>(
            //    name: "--packageId",
            //    description: "set package id");

            var idArgument = new Argument<string>(
                name: "id",
                description: "set package id");

            var versionOption = new Option<string>(
                name: "--packageVersion",
                description: "set version");

            var updateOption = new Option<bool>(
                name: "--update",
                description: "update all packages");

            var dryRunOption = new Option<bool>(
                name: "--dry-run",
                description: "just print the result");

            var descBuilder = new StringBuilder()
                .AppendFormat("Kraken Framework {0}", typeof(Program).Assembly.GetName().Version).AppendLine()
                .AppendLine("==================")
                .AppendFormat("Root:    {0}", rootDirectory).AppendLine();
            //.AppendFormat("Sources: {0}", string.Join(",", sources)).AppendLine();

            var rootCommand = new RootCommand(descBuilder.ToString());

            // kraken add --packageId packageId --packageVersion version
            var addCommand = new Command("add", "add new app") { nameArgument, idArgument, versionOption, dryRunOption };
            rootCommand.AddCommand(addCommand);
            var addCommandHandler = new AddCommandHandler(rootDirectory, packageManager, ct);
            addCommand.SetHandler(addCommandHandler.Handler, nameArgument, idArgument, versionOption, dryRunOption);



            // kranken run name (--update) -> run existing app
            var extraArgs = new Argument<string[]>("args", () => Array.Empty<string>(), "extra args");
            var runCommand = new Command("run", "run app") { nameArgument, updateOption, extraArgs};
            rootCommand.AddCommand(runCommand);
            var runCommandHandler = new RunCommandHandler(rootDirectory, packageManager, ct);
            runCommand.SetHandler(runCommandHandler.Handler, nameArgument, updateOption, extraArgs);

            // kranken update name
            var updateCommand = new Command("update", "update app") { nameArgument, dryRunOption };
            rootCommand.AddCommand(updateCommand);
            var updateCommandHandler = new UpdateCommandHandler(rootDirectory, packageManager, ct);
            updateCommand.SetHandler(updateCommandHandler.Handler, nameArgument, dryRunOption);

            // kranken upgrade name
            var upgradeCommand = new Command("upgrade", "upgrade app") { nameArgument, versionOption, dryRunOption };
            rootCommand.AddCommand(upgradeCommand);
            var upgradeCommandHandler = new UpgradeCommandHandler(rootDirectory, packageManager, ct);
            upgradeCommand.SetHandler(upgradeCommandHandler.Handler, nameArgument, versionOption, dryRunOption);

            // kraken list -> list all apps
            var listCommand = new Command("list", "list apps");
            rootCommand.AddCommand(listCommand);
            var listCommandHandler = new ListCommandHandler(rootDirectory);
            listCommand.SetHandler(listCommandHandler.Handler);

            var commandLineBuilder = new CommandLineBuilder(rootCommand);

            // https://learn.microsoft.com/en-us/dotnet/standard/commandline/use-middleware
            //commandLineBuilder.AddMiddleware(async (context, next) =>
            //{
            //    await next(context);
            //});

            // invoke help if no argumentc
            if (args.Length == 0)
            {
                args = ["--help"];
            }

            commandLineBuilder.UseDefaults();
            var parser = commandLineBuilder.Build() ?? throw new ArgumentNullException("parser");
            return await parser.InvokeAsync(args);

            //return await rootCommand.InvokeAsync(args);

        }

    }
}