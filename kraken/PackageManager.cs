﻿
using kraken.Models;
using NuGet.Common;
using NuGet.Configuration;
using NuGet.Frameworks;
using NuGet.Packaging;
using NuGet.Packaging.Core;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;
using NuGet.Versioning;
using System;
using System.Net;
using System.Threading;

namespace kraken
{
    public class PackageManager
    {
        private readonly NuGetFramework targetFramework;
        private readonly string appsDirectory;
        private readonly SourceCacheContext cache;
        private readonly SourceRepository packagesRepository;
        private readonly IEnumerable<SourceRepository> sourceRepositories;
        private readonly string packagesDirectory;
        //private readonly string localPackagesDirectory;

        public PackageManager(NuGetFramework targetFramework, string appsDirectory, SourceRepository packagesRepository, params SourceRepository[] sourceRepositories)
        {
            this.targetFramework = targetFramework;
            this.appsDirectory = appsDirectory;
            this.packagesRepository = packagesRepository;
            this.sourceRepositories = sourceRepositories;
            this.packagesDirectory = packagesRepository.PackageSource.Source; // null!;
            //this.localPackagesDirectory = Path.Combine(appDirectory, "packages");

            this.cache = new SourceCacheContext();
            //this.appDirectory = appDirectory;
        }

        public async Task<bool> UpdateAsync(App app, PackageList packages, CancellationToken ct)
        {

            var appVersion = VersionRange.Parse($"[{app.Version}]");

            // Dependencies vorab laden, um zu gewährleisten, dass wir auch nur in der gewünschten Range nach Updates suchen
            // TODO: Der Schritt könnte entfallen, wenn wir bei schreiben der <app>.config zusätzlich die VersionRange mit speichern würden
            Console.WriteLine("  Resolving dependencies...");
            var dependencies = await this.ResolveDependenciesAsync(app.Id, appVersion, ct);
            var dependenciesDict = dependencies.ToDictionary(k => k.Id, v => v.VersioRange, StringComparer.OrdinalIgnoreCase);

            bool updated = false;
            foreach (var package in packages.Packages)
            {
                if (package.Id == app.Id)
                {
                    if (package.Version != app.Version)
                    {
                        Console.WriteLine("  {0} {1} -> {2}", package.Id, package.Version, app.Version);
                        package.Version = app.Version;
                        updated = true;
                    }
                }
                else if (dependenciesDict.TryGetValue(package.Id, out var versionRange))
                {
                    var packageVersion = await FindBestMatchingPackageVersionAsync(sourceRepositories.Concat([packagesRepository]), package.Id, versionRange, ct);
                    if (packageVersion != null && packageVersion.ToString() != package.Version)
                    {
                        Console.WriteLine("  {0} {1} -> {2}", package.Id, package.Version, packageVersion);
                        package.Version = packageVersion.ToString();
                        updated = true;
                    }
                    else
                    {
                        Console.WriteLine("    {0} {1}", package.Id, package.Version);
                    }
                }

            }
            return updated;
        }

        public async Task<bool> SyncPackagesAsync(PackageList packages, PackageList localPackages, string appDirectory, bool update, CancellationToken ct)
        {

            //var localPacka
            bool result = false;
            foreach (var localPackage in localPackages.Packages.ToArray())
            {
                // uninstall packages if not in packages
                var package = packages.Packages.SingleOrDefault(x => x.Id == localPackage.Id);
                if (package == null)
                {
                    Console.WriteLine("Uninstall {0} {1}", localPackage.Id, localPackage.Version);
                    await UninstallPackageAsync(localPackage.Id, localPackage.Version, appDirectory, ct);
                    localPackages.Packages.Remove(localPackage);
                    result = true;
                }
            }

            foreach (var package in packages.Packages.ToArray())
            {
                // install package if necessary
                var packageFile = Path.Combine(packagesDirectory, $"{package.Id}.{package.Version}.nupkg");
                var localPackageFile = Path.Combine(appDirectory, "packages", $"{package.Id}.{package.Version}.nupkg");
                var localPackage = localPackages.Packages.SingleOrDefault(x => x.Id == package.Id);
                if (localPackage == null ||
                    localPackage.Version != package.Version ||
                    localPackage.TargetFramework != package.TargetFramework ||
                    (update && File.Exists(packageFile) && File.Exists(localPackageFile) && File.GetLastWriteTime(packageFile) > File.GetLastWriteTime(localPackageFile))
                )
                {

                    // uninstall previous version before installing
                    if (localPackage != null)
                    {
                        Console.WriteLine("Update {0} {1} -> {2}", package.Id, localPackage.Version, package.Version);
                        await UninstallPackageAsync(localPackage.Id, localPackage.Version, appDirectory, ct);
                    }
                    else
                    {
                        Console.WriteLine("Install {0} {1}", package.Id, package.Version);
                    }
                    await InstallPackageAsync(package.Id, package.Version, appDirectory, ct);

                    if (localPackage == null)
                    {
                        localPackage = new Package
                        {
                            Id = package.Id,
                            Version = package.Version,
                            TargetFramework = targetFramework.ToString()
                        };
                        localPackages.Packages.Add(localPackage);
                    }
                    else
                    {
                        localPackage.Version = package.Version;
                        localPackage.TargetFramework = targetFramework.ToString();
                    }

                    result = true;
                }
            }
            return result;
        }
        
        public async Task<IEnumerable<PackageDependency>> ResolveDependenciesAsync(string id, VersionRange versionRange, CancellationToken ct)
        {
            var dependencies = new Dictionary<string, VersionRange>();
            await ResolveDependenciesAsync(id, versionRange, dependencies, ct);
            return dependencies.Select(x => new PackageDependency { Id = x.Key, VersioRange = x.Value }).ToArray();
        }

        public async Task<PackageList> CreatePackageListAsync(string id, string version, CancellationToken ct)
        {
            // create a list of all packages and their package dependencies

            var packageList = new PackageList();

            packageList.Packages.Add(new Package { Id = id, Version = version, TargetFramework = targetFramework.ToString() });

            var versionRange = VersionRange.Parse($"[{version}]");
            Console.WriteLine("  Resolving dependencies...");
            var dependencies = await ResolveDependenciesAsync(id, versionRange, ct); //  dependencies, ct);
            foreach (var dependency in dependencies)
            {
                var dependencyVersion = await FindBestMatchingPackageVersionAsync(sourceRepositories.Concat([this.packagesRepository]), dependency.Id, dependency.VersioRange, ct) ??
                                        throw new ArgumentException($"Unable to find package {id} {versionRange}");
                Console.WriteLine("  {0} {1}", dependency.Id, dependencyVersion);
                packageList.Packages.Add(new Package { Id = dependency.Id, Version = dependencyVersion.ToString(), TargetFramework = targetFramework.ToString() });
            }

            return packageList;
        }

        public async Task<NuGetVersion> FindVersionAsync(string id, CancellationToken ct)
        {
            var versionRange = VersionRange.AllStable;
            var version = await FindBestMatchingPackageVersionAsync(this.sourceRepositories.Concat([this.packagesRepository]), id, versionRange, ct) ??
                          throw new ArgumentException($"Unable to find version for {id}");
            return version;
        }

        private async Task InstallPackageAsync(string id, string version, string appDirectory, CancellationToken ct)
        {
            using var packageReader = await GetPackageReaderAsync(id, NuGetVersion.Parse(version), packagesDirectory, ct);
            await InstallPackageAsync(packageReader, appDirectory, ct);

            // keep a local copy of the package
            var packageFilename = $"{id}.{version}.nupkg";
            var packagePath = Path.Combine(packagesDirectory, packageFilename);
            var localPackagePath = Path.Combine(appDirectory, "packages", packageFilename);
            await packageReader.CopyNupkgAsync(localPackagePath, ct);

            // copy created / modified for update check
            File.SetCreationTimeUtc(localPackagePath, File.GetCreationTime(packagePath));
            File.SetLastWriteTime(localPackagePath, File.GetLastWriteTime(packagePath));
        }

        private async Task InstallPackageAsync(PackageArchiveReader packageReader, string appDirectory, CancellationToken ct)
        {
            
            var libItems = await packageReader.GetLibItemsAsync(ct); // Gets all library items in the package
            await InstallItemsAsync(packageReader, libItems, Path.Combine(appDirectory, "bin"), ct);

            var contentItems = await packageReader.GetContentItemsAsync(ct); // Gets all content items in the package
            await InstallItemsAsync(packageReader, contentItems, appDirectory, ct);

        }

        private async Task InstallItemsAsync(PackageArchiveReader packageReader, IEnumerable<FrameworkSpecificGroup> items, string directory, CancellationToken ct)
        {

            var frameworkReducer = new FrameworkReducer();
            // Get the best matching framework that is supported by the package, given your target framework
            var nearestFramework = frameworkReducer.GetNearest(this.targetFramework, items.Select(x => x.TargetFramework));
            if (nearestFramework != null)
            {
                // Get the list of all files for the nearest framework
                var nearestItems = items.FirstOrDefault(x => x.TargetFramework.Equals(nearestFramework));

                if (nearestItems != null)
                {
                    foreach (var item in nearestItems.Items)
                    {

                        // Construct the destination path by getting the part after 'lib/<targetFramework>/'
                        var pathParts = item.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                        if (pathParts[0].Equals("content", StringComparison.OrdinalIgnoreCase))
                        {
                            // Skip 'content' segment
                            pathParts = pathParts.Skip(1).ToArray();
                        }
                        else if (pathParts[0].Equals("lib", StringComparison.OrdinalIgnoreCase))
                        {
                            // Skip 'lib' and framework segments to get the relative path
                            pathParts = pathParts.Skip(2).ToArray();
                        }
                        var relativePath = Path.Combine(pathParts);
                        var destinationPath = Path.Combine(directory, relativePath);

                        // Ensure the directory for the destination file exists
                        var destinationDirectory = Path.GetDirectoryName(destinationPath) ?? directory;
                        Directory.CreateDirectory(destinationDirectory);

                        // Extract the file from the package to the destination path
                        using (var sourceStream = packageReader.GetStream(item))
                        using (var targetStream = File.Create(destinationPath))
                        {
                            await sourceStream.CopyToAsync(targetStream);
                        }

                    }
                }

            }
        }

        private async Task UninstallPackageAsync(string id, string version, string appDirectory, CancellationToken ct)
        {
            var localPackagesDirectory = Path.Combine(appDirectory, "packages");
            using (var packageReader = await GetPackageReaderAsync(id, NuGetVersion.Parse(version), localPackagesDirectory, ct))
            {
                await UninstallPackageAsync(packageReader, appDirectory, ct);
            }

            // delete local copy of the package
            var packageFilename = $"{id}.{version}.nupkg";
            var localPackagePath = Path.Combine(localPackagesDirectory, packageFilename);
            if (File.Exists(localPackagePath))
            {
                File.Delete(localPackagePath);
            }
        }

        private async Task UninstallPackageAsync(PackageArchiveReader packageReader, string appDirectory, CancellationToken ct)
        {
            var libItems = await packageReader.GetLibItemsAsync(ct); // Gets all library items in the package
            await UninstallItemsAsync(packageReader, libItems, Path.Combine(appDirectory, "bin"), ct);

            var contentItems = await packageReader.GetLibItemsAsync(ct); // Gets all content items in the package
            await UninstallItemsAsync(packageReader, contentItems, appDirectory, ct);


        }

        private Task UninstallItemsAsync(PackageArchiveReader packageReader, IEnumerable<FrameworkSpecificGroup> items, string directory, CancellationToken ct)
        {

            var frameworkReducer = new FrameworkReducer();

            // Get the best matching framework that is supported by the package, given your target framework
            var nearestFramework = frameworkReducer.GetNearest(this.targetFramework, items.Select(x => x.TargetFramework));
            if (nearestFramework != null)
            {
                // Get the list of all files for the nearest framework
                var nearestItems = items.FirstOrDefault(x => x.TargetFramework.Equals(nearestFramework));

                if (nearestItems != null)
                {
                    foreach (var item in nearestItems.Items)
                    {

                        // Construct the destination path by getting the part after 'lib/<targetFramework>/'
                        var pathParts = item.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                        if (pathParts[0].Equals("content", StringComparison.OrdinalIgnoreCase))
                        {
                            // Skip 'content' segment
                            pathParts = pathParts.Skip(1).ToArray();
                        }
                        else if (pathParts[0].Equals("lib", StringComparison.OrdinalIgnoreCase))
                        {
                            // Skip 'lib' and framework segments to get the relative path
                            pathParts = pathParts.Skip(2).ToArray();
                        }
                        var relativePath = Path.Combine(pathParts);
                        var destinationPath = Path.Combine(directory, relativePath);
                        if (File.Exists(destinationPath))
                        {
                            File.Delete(destinationPath);
                        }

                    }
                }

            }

            return Task.CompletedTask;
        }

        private async Task<PackageArchiveReader> GetPackageReaderAsync(string id, NuGetVersion version, string packagesDirectory, CancellationToken ct)
        {
            var packageFilename = $"{id}.{version}.nupkg";
            var packagePath = Path.Combine(packagesDirectory, packageFilename);
            //var resource = await this.packagesRepository.GetResourceAsync<FindPackageByIdResource>(ct);
            //var exists = await resource.DoesPackageExistAsync(id, version, cache, NullLogger.Instance, ct);
            if (!File.Exists(packagePath))
            {
                await DownloadPackageAsync(id, version, packagePath, ct);
            }

            return new PackageArchiveReader(packagePath);
        }

        private async Task DownloadPackageAsync(string id, NuGetVersion version, string packagePath, CancellationToken ct)
        {
            bool success = false;
            foreach (var repository in this.sourceRepositories)
            {
                var resource = await repository.GetResourceAsync<FindPackageByIdResource>();

                using var packageStream = new FileStream(packagePath, FileMode.Create, FileAccess.Write);

                success = await resource.CopyNupkgToStreamAsync(
                    id,
                    version,
                    packageStream,
                    cache,
                    NullLogger.Instance,
                    ct);
                if (success)
                {
                    break;
                }
            }
            if (!success)
            {
                throw new ArgumentException("Failed to download the package.");
            }
        }

        private async Task<NuGetVersion?> FindBestMatchingPackageVersionAsync(IEnumerable<SourceRepository> repositories, string id, VersionRange versionRange, CancellationToken ct)
        {

            // Get all versions of the package
            var allVersions = new HashSet<NuGetVersion>();
            foreach (var repository in repositories)
            {
                var findPackageByIdResource = await repository.GetResourceAsync<FindPackageByIdResource>();
                IEnumerable<NuGetVersion> versions = await findPackageByIdResource.GetAllVersionsAsync(id, cache, NullLogger.Instance, ct);
                allVersions.UnionWith(versions);
            }
            // Filter versions by the provided version range and select the best match (highest version in range)
            var bestMatchVersion = allVersions
                .Where(version => !version.IsPrerelease)
                .Where(version => versionRange.Satisfies(version))
                .OrderByDescending(version => version)
                .FirstOrDefault();
            return bestMatchVersion;
        }

        private async Task ResolveDependenciesAsync(string id, VersionRange versionRange, Dictionary<string, VersionRange> resolvedDependencies, CancellationToken ct)
        {

            //Console.WriteLine("  Resolving dependencies for {0} {1}", id, versionRange);
            var dependencyInfo = await GetPackageDependencyInfoAsync(this.sourceRepositories.Concat([this.packagesRepository]), id, versionRange, ct) ??
                                 throw new ArgumentException($"Unable to get package dependency info {id} {versionRange}");
            foreach (var dependency in dependencyInfo.Dependencies)
            {
                if (resolvedDependencies.ContainsKey(dependency.Id))
                {
                    // If dependency already exists, find version range intersection
                    resolvedDependencies[dependency.Id] = VersionRange.CommonSubSet(new[] { resolvedDependencies[dependency.Id], dependency.VersionRange });
                }
                else
                {
                    resolvedDependencies.Add(dependency.Id, dependency.VersionRange);
                    await ResolveDependenciesAsync(dependency.Id, dependency.VersionRange, resolvedDependencies, ct);
                }
            }
        }

        private async Task<SourcePackageDependencyInfo?> GetPackageDependencyInfoAsync(IEnumerable<SourceRepository> repositories, string id, VersionRange versionRange, CancellationToken ct)
        {

            NuGetVersion? bestVersion = null;
            SourceRepository? bestVersionRepository = null;

            foreach (var repository in repositories)
            {
                var version = await FindBestMatchingPackageVersionAsync([repository], id, versionRange, ct);
                if (version != null && (bestVersion == null || version != bestVersion))
                {
                    bestVersion = version;
                    bestVersionRepository = repository;
                }
            }

            if (bestVersion !=  null && bestVersionRepository != null)
            {
                var packageIdentity = new PackageIdentity(id, bestVersion);
                var dependencyInfoResource = await bestVersionRepository.GetResourceAsync<DependencyInfoResource>();
                var dependencyInfo = await dependencyInfoResource.ResolvePackage(packageIdentity, targetFramework, cache, NullLogger.Instance, ct);
                return dependencyInfo;
            }

            return null;
        }

    }

    public class PackageDependency
    {
        public string Id { get; set; } = null!;
        public VersionRange VersioRange { get; set; } = null!;
    }

}