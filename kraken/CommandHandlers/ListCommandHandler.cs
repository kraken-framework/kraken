﻿using System.CommandLine.Invocation;
using kraken.Models;

namespace kraken.CommandHandlers
{
    internal class ListCommandHandler : CommandHandlerBase
    {

        public ListCommandHandler(string rootDirectory) : base(rootDirectory)
        {
        }

        internal void Handler()
        {

            var appsDirectory = Path.Combine(rootDirectory, "apps");
            var files = Directory.GetFiles(appsDirectory, "*.app");
            var apps = files.Select(x => new { Name = Path.GetFileNameWithoutExtension(x), App = Utils.DeserializeXml<App>(x) }).Select(x => new { x.Name, x.App.Id, x.App.Version }).ToArray();

            // Find maximum lengths for each column to align the table
            int nameMaxLength = Math.Max("Name".Length, apps.Length  > 0 ? apps.Max(o => o.Name.Length) : 0);
            int idMaxLength = Math.Max("Id".Length, apps.Length > 0 ? apps.Max(o => o.Id.ToString().Length) : 0);
            int versionMaxLength = Math.Max("Version".Length, apps.Length > 0 ? apps.Max(o => o.Version.Length) : 0);

            // Print table header
            Console.WriteLine($"{"Name".PadRight(nameMaxLength)}  {"Id".PadRight(idMaxLength)}  {"Version".PadRight(versionMaxLength)}");
            Console.WriteLine(new string('-', nameMaxLength + idMaxLength + versionMaxLength + 4)); // Adjust separator length

            // Print each object in the list
            foreach (var app in apps)
            {
                Console.WriteLine($"{app.Name.PadRight(nameMaxLength)}  {app.Id.ToString().PadRight(idMaxLength)}  {app.Version.PadRight(versionMaxLength)}");
            }
        }
    }
}