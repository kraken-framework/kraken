﻿using kraken.Models;
using NuGet.Common;
using NuGet.Configuration;
using NuGet.Frameworks;
using NuGet.Packaging.Core;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;
using NuGet.Versioning;
using System;
using System.CommandLine.Invocation;
using System.Xml.Linq;

namespace kraken.CommandHandlers
{
    internal class AddCommandHandler : CommandHandlerBase
    {
        private readonly PackageManager packageManager;
        private readonly CancellationToken ct;

        public AddCommandHandler(string rootDirectory, PackageManager packageManager, CancellationToken ct) : base(rootDirectory)
        {
            this.packageManager = packageManager;
            this.ct = ct;
        }

        internal async Task Handler(string name, string id, string version, bool dryRun)
        {
            Console.WriteLine("Add {0} {1} {2}", name, id, version);
            ArgumentNullException.ThrowIfNull(name, nameof(name));
            ArgumentNullException.ThrowIfNull(id, nameof(id));
            //ArgumentNullException.ThrowIfNull(version, nameof(version));

            var appFile = Path.Combine(this.appsDirectory, $"{name}.app");
            var packagesFile = Path.Combine(this.appsDirectory, $"{name}.config");
            if (File.Exists(appFile)) throw new ArgumentException("App already exists");

            if (string.IsNullOrEmpty(version))
            {
                version = (await packageManager.FindVersionAsync(id, ct)).ToString();
            }
            var packages = await packageManager.CreatePackageListAsync(id, version, ct);

            var app = new App { Id = id, Version = version };
            if (!dryRun)
            {
                Utils.SerializeXml(app, appFile);
                Utils.SerializeXml(packages, packagesFile);
            }
        }

    }
}