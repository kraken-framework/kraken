﻿using kraken.Models;
using Microsoft.VisualBasic.FileIO;
using NuGet.Frameworks;
using NuGet.Packaging;
using NuGet.Protocol.Core.Types;
using System;
using System.CommandLine.Invocation;
using System.Reflection;
using System.Runtime.Versioning;
using System.Text.RegularExpressions;

namespace kraken.CommandHandlers
{
    internal class RunCommandHandler : CommandHandlerBase
    {
        private readonly PackageManager packageManager;
        private readonly CancellationToken ct;

        public RunCommandHandler(string rootDirectory, PackageManager packageManager, CancellationToken ct) : base(rootDirectory)
        {
            this.packageManager = packageManager;
            this.ct = ct;
        }


        public async Task Handler(string name, bool update, string[] extraArgs)
        {
            Console.WriteLine("Run {0}", name);
            ArgumentNullException.ThrowIfNull(name, nameof(name));

            var packagesDirectory = Path.Combine(rootDirectory, "packages");

            var appFile = Path.Combine(appsDirectory, $"{name}.app");
            var packagesFile = Path.Combine(appsDirectory, $"{name}.config");

            var appDirectory = Path.Combine(appsDirectory, name);
            Directory.CreateDirectory(appDirectory);

            var localPackagesFile = Path.Combine(appDirectory, $"packages.config");
            var localPackagesDirectory = Path.Combine(appDirectory, "packages");
            Directory.CreateDirectory(localPackagesDirectory);

            if (!File.Exists(appFile)) throw new ArgumentException($"{appFile} not found");
            if (!File.Exists(packagesFile)) throw new ArgumentException($"{packagesFile} not found");

            var app = Utils.DeserializeXml<App>(appFile);
            var packages = Utils.DeserializeXml<PackageList>(packagesFile);
            var localPackages = File.Exists(localPackagesFile) ? Utils.DeserializeXml<PackageList>(localPackagesFile) : new PackageList();

            if (update)
            {
                // if the timestamp in the packages folder is newer than the timestamp in the local packages folder
                // we update the version in the packages file. this is only needed for debugging
                // TODO: after we save the VersionRange in the packages, we can narrow this down to a matching version
                foreach (var package in packages.Packages)
                {
                    var latestPackage = GetLatestPackage(packagesDirectory, package);
                    if (latestPackage.Version != package.Version)
                    {
                        package.Version = latestPackage.Version;
                    }
                }

            }

            if (await packageManager.SyncPackagesAsync(packages, localPackages, appDirectory, update, ct))
            {
                Utils.SerializeXml(localPackages, localPackagesFile);
            }

            var bootStrapper = new BootStrapper(appDirectory, app.Id);
            await bootStrapper.RunAsync(extraArgs, ct);
        }

        private Package GetLatestPackage(string packagesDirectory, Package package)
        {
            var packageFilePaths = Directory.GetFiles(packagesDirectory, $"{package.Id}.*.nupkg");

            foreach (var packageFilePath in packageFilePaths.OrderByDescending(x => File.GetLastWriteTime(x)))
            {
                using (var reader = CreatePackageArchiveReader(packageFilePath))
                {
                    var nuspecReader = reader.NuspecReader;
                    var packageId = nuspecReader.GetId();
                    var packageVersion = nuspecReader.GetVersion();

                    if (packageId == package.Id)
                    {
                        return new Package { Id = packageId, Version = packageVersion.ToString() };
                    }
                }
            }
            return package;
        }

        private PackageArchiveReader CreatePackageArchiveReader(string packageFilePath)
        {
            try
            {
                return new PackageArchiveReader(packageFilePath);
            }
            catch (InvalidDataException)
            {
                // Unhandled exception: System.IO.InvalidDataException: The file is not a valid nupkg. File path: C:\..\kraken\dist\packages\package.id.1.0.0.nupkg
                // --->System.IO.InvalidDataException: Central Directory corrupt.
                // --->System.IO.IOException: Falscher Parameter. : 'C:\...\kraken\dist\packages\package.id.1.0.0.nupkg'

                // TODO: Delete and Download the file. Then return the value
                throw;
            }
        }
    }
}