﻿using kraken.Models;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.CommandHandlers
{
    internal abstract class CommandHandlerBase
    {
        protected readonly string rootDirectory;
        protected readonly string appsDirectory;

        public CommandHandlerBase(string rootDirectory)
        {
            this.rootDirectory = rootDirectory;
            this.appsDirectory = Path.Combine(rootDirectory, "apps");
        }

        //public void Init(string name)
        //{

        //    var appsDirectory = Path.Combine(rootDirectory, "apps");
        //    var appFile = Path.Combine(appsDirectory, $"{name}.app");
        //    var nugetConfigFile = Path.Combine(rootDirectory, "NuGet.Config");

        //    var packagesFile = Path.Combine(appsDirectory, $"{name}.config");

        //    var appDirectory = Path.Combine(appsDirectory, name);
        //    Directory.CreateDirectory(appDirectory);

        //    var packagesDirectory = Path.Combine(rootDirectory, "packages");
        //    Directory.CreateDirectory(packagesDirectory);

        //    var localPackagesDirectory = Path.Combine(appDirectory, "packages");
        //    Directory.CreateDirectory(localPackagesDirectory);

        //    var targetFramework = Utils.GetTargetFramework();
        //    var nugetConfig = NuGetConfig.Deserialize(nugetConfigFile);
        //    var sourceRepositories = NuGetConfig.GetSourceRepositories(nugetConfig);
        //    var packagesRepository = Repository.Factory.GetCoreV3(packagesDirectory);

        //    this.packageManager = new PackageManager(
        //        targetFramework,
        //        appsDirectory,
        //        packagesRepository,
        //        sourceRepositories.ToArray());
        //}
    }
}
