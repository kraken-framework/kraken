﻿
using kraken.Models;
using System;

namespace kraken.CommandHandlers
{
    internal class UpdateCommandHandler : CommandHandlerBase
    {
        private readonly PackageManager packageManager;
        private readonly CancellationToken ct;

        public UpdateCommandHandler(string rootDirectory, PackageManager packageManager, CancellationToken ct) : base(rootDirectory)
        {
            this.packageManager = packageManager;
            this.ct = ct;
        }

        internal async Task Handler(string name, bool dryRun)
        {
            Console.WriteLine("Update {0}", name);
            ArgumentNullException.ThrowIfNull(name, nameof(name));

            var appFile = Path.Combine(appsDirectory, $"{name}.app");
            var packagesFile = Path.Combine(appsDirectory, $"{name}.config");

            if (!File.Exists(appFile)) throw new ArgumentException($"{appFile} not found");
            if (!File.Exists(packagesFile)) throw new ArgumentException($"{packagesFile} not found");

            var app = Utils.DeserializeXml<App>(appFile);
            var packages = Utils.DeserializeXml<PackageList>(packagesFile);

            // Bei Update bleibt die App erhalten und es werden nur die Dependencies aktualisiert
            //var appPackage = packages.Packages.Single(x => x.Id == app.Id);
            //packages.Packages.Remove(appPackage);

            Console.WriteLine("checking for package updates...");
            if (await packageManager.UpdateAsync(app, packages, ct) && !dryRun)
            {
                Utils.SerializeXml(packages, packagesFile);
            }

        }
    }
}