﻿
using kraken.Models;

namespace kraken.CommandHandlers
{
    internal class UpgradeCommandHandler : CommandHandlerBase
    {
        private readonly PackageManager packageManager;
        private readonly CancellationToken ct;

        public UpgradeCommandHandler(string rootDirectory, PackageManager packageManager, CancellationToken ct) : base(rootDirectory)
        {
            this.packageManager = packageManager;
            this.ct = ct;
        }

        internal async Task Handler(string name, string version, bool dryRun)
        {
            Console.WriteLine("Upgrade {0} {1}", name, version);
            ArgumentNullException.ThrowIfNull(name, nameof(name));

            var appFile = Path.Combine(appsDirectory, $"{name}.app");
            var packagesFile = Path.Combine(appsDirectory, $"{name}.config");

            if (!File.Exists(appFile)) throw new ArgumentException($"{appFile} not found");
            if (!File.Exists(packagesFile)) throw new ArgumentException($"{packagesFile} not found");

            var app = Utils.DeserializeXml<App>(appFile);
            var packages = Utils.DeserializeXml<PackageList>(packagesFile);

            if (string.IsNullOrEmpty(version))
            {
                version = (await packageManager.FindVersionAsync(app.Id, ct)).ToString();
            }
            app.Version = version;

            Console.WriteLine("checking for package updates...");
            if (await packageManager.UpdateAsync(app, packages, ct) && !dryRun)
            {
                Utils.SerializeXml(app, appFile);
                Utils.SerializeXml(packages, packagesFile);
            }

        }
    }
}