﻿using kraken.runtime;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.service
{
    public class DefaultApplicationContext : IApplicationContext
    {
        private IServiceCollection services;
        private IServiceProvider? serviceProvider;
        public DefaultApplicationContext(IServiceCollection services)
        {
            this.services = services;
        }

        private IServiceProvider ServiceProvider
        {
            get
            {
                if (this.serviceProvider == null)
                {
                    this.serviceProvider = services.BuildServiceProvider();
                }
                return this.serviceProvider;
            }
        }
        //private Lazy<ServiceProvider> serviceProvider = new Lazy<ServiceProvider>(() => this.services.BuildServiceProvider());

        public void Use(Type serviceType, Type implementationType, Scope scope = Scope.Transient)
        {
            services.RemoveAll(serviceType);
            Register(serviceType, implementationType, scope);
        }

        public void Use<TService, TImplementation>(Scope scope = Scope.Transient)
            where TService : class
            where TImplementation : class, TService
        {
            services.RemoveAll<TService>();
            Register<TService, TImplementation>(scope);
        }

        public void Register(Type serviceType, Type implementationType, Scope scope = Scope.Transient)
        {
            switch (scope)
            {
                case Scope.Singleton:
                    services.AddSingleton(serviceType, implementationType);
                    break;
                case Scope.Transient:
                default:
                    services.AddTransient(serviceType, implementationType);
                    break;
            }
        }

        public void Register<TService, TImplementation>(Scope scope = Scope.Transient)
            where TService : class
            where TImplementation : class, TService
        {
            switch (scope)
            {
                case Scope.Singleton:
                    services.AddSingleton<TService, TImplementation>();
                    break;
                case Scope.Transient:
                default:
                    services.AddTransient<TService, TImplementation>();
                    break;
            }
        }

        public TService? GetService<TService>()
        {
            return this.ServiceProvider.GetService<TService>();
        }

        public object? GetService(Type type)
        {
            return this.ServiceProvider.GetService(type);
        }

        public IEnumerable<TService> GetServices<TService>()
        {
            return this.ServiceProvider.GetServices<TService>();
        }

        public IEnumerable<object?> GetServices(Type type)
        {
            return this.ServiceProvider.GetServices(type);
        }
    }
}
