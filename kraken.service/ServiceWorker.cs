﻿using kraken.runtime;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace kraken.service
{
    internal class ServiceWorker : IHostedService
    {
        private readonly IService service;

        public ServiceWorker(IService service) 
        {
            this.service = service;
            // comment
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            return service.StartAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return service.StopAsync(cancellationToken);
        }
    }
}
