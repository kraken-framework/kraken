﻿using kraken.runtime;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.service
{
    public abstract class ServiceApplicationBase : IApplication
    {
        private readonly HostApplicationBuilder builder;
        private IHost? host;

        public ServiceApplicationBase(string[] args)
        {
            this.builder = Host.CreateApplicationBuilder(args);
        }

        public virtual Task<IApplicationContext> InitAsync(CancellationToken ct)
        {
            Console.WriteLine("InitAsync");
             
            builder.Logging.ClearProviders();
            builder.Logging.AddConsole();

            //builder.Services.AddHostedService<Worker>();

            var context = new DefaultApplicationContext(builder.Services) as IApplicationContext;

            return Task.FromResult(context);
        }

        public virtual async Task StartAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("StartAsync");

            foreach (var service in context.GetServices<IService>())
            {
                builder.Services.AddHostedService(services => new ServiceWorker(service));
            }

            var host = builder.Build();
            await host.StartAsync(ct);
            this.host = host;
        }

        public virtual async Task StopAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("StopAsync");
            if (this.host != null)
            {
                await this.host.StopAsync(ct);
            }
        }
    }
}
