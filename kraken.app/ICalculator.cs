﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.app
{
    public interface ICalculator
    {
        public int Add(int a, int b);
    }
}
