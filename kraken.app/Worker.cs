using kraken.runtime;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace kraken.app
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IEnumerable<ICalculator> _calculators;

        public Worker(ILogger<Worker> logger, IEnumerable<ICalculator> calculators)
        {
            _logger = logger;
            _calculators = calculators;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_logger.IsEnabled(LogLevel.Information))
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                }
                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
