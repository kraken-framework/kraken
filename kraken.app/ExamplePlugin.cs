﻿using kraken.runtime;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.app
{
    public class ExamplePlugin : IPlugin, IPluginStart, IPluginStop
    {
        public Task LoadAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("ExamplePlugin loaded");
            context.Register<ICalculator, Calculator>();
            context.Register<ICalculator, Calculator2>();
            context.Register<IHostedService, Worker>();
            return Task.CompletedTask;
        }

        public Task BeforeStartAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("ExamplePlugin after start");
            return Task.CompletedTask;
        }

        public Task AfterStartAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("ExamplePlugin after start");
            return Task.CompletedTask;
        }

        public Task BeforeStopAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("ExamplePlugin after start");
            return Task.CompletedTask;
        }

        public Task AfterStopAsync(IApplicationContext context, CancellationToken ct)
        {
            Console.WriteLine("ExamplePlugin after stop");
            return Task.CompletedTask;
        }

    }
}
