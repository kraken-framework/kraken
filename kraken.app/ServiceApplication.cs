﻿using kraken.runtime;
using kraken.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kraken.app
{
    public class ServiceApplication : ServiceApplicationBase
    {
        public ServiceApplication(string[] args) : base(args)
        {
        }
        public override Task<IApplicationContext> InitAsync(CancellationToken ct)
        {
            return base.InitAsync(ct);
        }
        public override Task StartAsync(IApplicationContext context, CancellationToken ct)
        {
            return base.StartAsync(context, ct);
        }
        public override Task StopAsync(IApplicationContext context, CancellationToken ct)
        {
            return base.StopAsync(context, ct);
        }
    }
}
